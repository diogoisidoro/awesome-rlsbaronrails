Rails.application.routes.draw do
  get '/cocktails/random', to: 'cocktails#random'
  
  resources :cocktails
  resources :categories
  resources :ingredients
  resources :tags


end
