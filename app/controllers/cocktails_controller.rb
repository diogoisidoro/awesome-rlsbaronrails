class CocktailsController < ApplicationController
	ATTRIBUTES = [:tags, :ingredients, :category]

	def random
		@cocktails = Cocktail.order(Arel.sql('RANDOM()')).first
		render json: @cocktails, include: ATTRIBUTES
	end

	def index
		@cocktails = get_cocktails_by_params(params)
		render json: @cocktails,  include: ATTRIBUTES
	end	
	
	def show
    @cocktail = Cocktail.find(params[:id])
		render json: @cocktail, include: ATTRIBUTES
	end

	private

	def get_cocktails_by_params(params)
		case 
		# Search by initial letter
		when params[:starts_with]
			return Cocktail.where('name LIKE :prefix', prefix: "#{params[:starts_with]}%")

		# Search by query
		when params[:q]
			return Cocktail.where('name LIKE :query', query: "%#{params[:q]}%")
		
		# Search by ingredient name
		when params[:ingredient]
			return Cocktail.joins(:ingredients).where("ingredients.name LIKE :ingredient", ingredient:"%#{params[:ingredient]}%" )
		

		when params[:category_id]
			return Cocktail.joins(:category).where("categories.id = #{params[:category_id]}")

		else
			return Cocktail.all
		end
	

	end

end