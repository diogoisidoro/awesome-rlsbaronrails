class Cocktail < ApplicationRecord
	validates :name, presence: true, uniqueness: true
	has_and_belongs_to_many :tags
	has_and_belongs_to_many :ingredients
	belongs_to :category
end
