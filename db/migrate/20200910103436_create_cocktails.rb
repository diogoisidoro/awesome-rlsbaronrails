class CreateCocktails < ActiveRecord::Migration[6.0]
  def change
    create_table :cocktails do |t|
      t.string :name, index: { unique: true }, null: false
      t.integer :cdb_id, null: false
      t.string :alternate_name
      t.references :category, null: false, foreign_key: true
      t.string :image
      t.string :instructions
      t.string :measures

      t.timestamps
    end
  end
end
