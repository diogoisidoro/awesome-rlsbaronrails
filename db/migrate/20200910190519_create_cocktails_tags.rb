class CreateCocktailsTags < ActiveRecord::Migration[6.0]
  def change
    create_table :cocktails_tags do |t|
      t.references :tag, null: false, foreign_key: true
      t.references :cocktail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
